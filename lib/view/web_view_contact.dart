import 'dart:io';

import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebviewContact extends StatefulWidget {
  WebviewContact({Key? key}) : super(key: key);
  @override
  WebviewContactState createState() => WebviewContactState();
}

class WebviewContactState extends State<WebviewContact> {
  @override
  void initState() {
    super.initState();
    // Enable virtual display.
    // if (Platform.isAndroid) WebView.platform = AndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: WebView(
        initialUrl: 'https://puspa.mwellplus.com/contact/',
        javascriptMode: JavascriptMode.unrestricted,
      ),
    );
    // return Scaffold(
    //     appBar: AppBar(
    //       title: Text("zz"),
    //     ),
    //     body: SingleChildScrollView(
    //       child: Text("data"),
    //       // child: WebView(
    //       //   initialUrl: 'https://flutter.dev',
    //       // ),
    //     ));
    // return WebView(
    //   initialUrl: 'https://flutter.dev',
    // );
  }
}
